# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Prereq: Service Principal or AD User must have Directory Read role permissions from Azure Active Directory
* This Powershell script allows Azure AD Service Principal or User to add other AD Users to Azure SQL as a DB Owner
* Using this Open Source Library: https://github.com/AzureAD/MSAL.PS

### How do I get set up? ###

* Prereq: Must be logged in with az login
* Pull down repo 
* Open Powershell console run with Administrator (Windows)
* Navigate to pulled down repo directory
* Execute Example: .\Set-AzADUserSQL.ps1 -AzADUser [AD USER TO ADD] -AzADAppId [AD USER CLIENT ID] -SQLServerName [AZURE SQL SERVER NAME] -databaseName [AZURE SQL DB NAME]

### Who do I talk to? ###

* Please contact Johnny Kang for any questions or comments regarding this repo.  Thank you.