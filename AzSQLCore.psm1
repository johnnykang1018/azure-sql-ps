﻿Function Get-Token {
    Param (
        [string]$clientId,
        [string]$clientSecret,
        [string]$tenantId,
        [string]$scopes
    )

    LogMessage -message "Get TOKEN..."
    $result = Get-MsalToken -ClientId $clientId -ClientSecret (ConvertTo-SecureString $clientSecret -AsPlainText -Force) -TenantId $tenantId -Scopes $scopes -ErrorAction SilentlyContinue
    $token = $result.AccessToken

    return $token
}

Function Get-ApplicationId {
    Param (
        [string]$AzADUser
    )

    LogMessage -message "GET APP ID FOR USER: $AzADUser..."

    $appServiceServicePrincipal = (Get-AzADServicePrincipal -SearchString $AzADUser -WarningAction Ignore).ApplicationId

    return $appServiceServicePrincipal
}

Function Get-SQLConnectionString {
    Param (
        [string]$SQLServerName,
        [string]$databaseName,
        [string]$token
    )

    LogMessage -message "PREPARING SQL CONNECTION STRING..."

    $conn = New-Object System.Data.SqlClient.SQLConnection 
    $conn.ConnectionString = "Data Source=$SQLServerName.database.windows.net;Initial Catalog=$databaseName;Connect Timeout=30"
    $conn.AccessToken = $token

    return $conn
}

Function Create-AzADUserAsDBOwner {
    Param (
        [object]$conn,
        [string]$appServiceServicePrincipal,
        [string]$AzADUser
    )

try
{
    LogMessage -message "OPENING SQL CONNECTION..."
    $conn.Open() 

    LogMessage -message "CONVERTING TO UNIQUE ID..."

$query =@"
SELECT CONVERT(VARCHAR(1000), CAST(CAST('$($appServiceServicePrincipal)' AS UNIQUEIDENTIFIER) AS VARBINARY(16)),1);
"@

        $command = $conn.CreateCommand()
        $command.CommandText = $query
        $appServiceServicePrincipalSid = $command.ExecuteScalar()

    LogMessage -message "ADDING AZURE AD USER & ASSIGNING PERMISSION..."

$query =@"
            IF NOT EXISTS (
                SELECT  [name]
                FROM    sys.database_principals
                WHERE   [name] = '$($AzADUser)'
            )
            BEGIN
                CREATE USER [$($AzADUser)] WITH SID=$($appServiceServicePrincipalSid), TYPE=E
                ALTER ROLE db_owner ADD MEMBER [$($AzADUser)];
            END
"@

    $command = New-Object -TypeName System.Data.SqlClient.SqlCommand($query, $conn)       
    $command.ExecuteNonQuery() | Out-Null
}
finally
{
    LogMessage -message "CLOSING SQL CONNECTION..."
    $conn.Close()
}
}

Function LogMessage {
    Param (
        [string]$message
    )

    $dateTimeStamp = Get-Date -Format "[yyyy-MM-dd HH:mm:ss]"

    Write-Host "$dateTimeStamp $message"
}

Function Load-Module {
    Param (
        [string]$m
    )

    # If module is imported say that and do nothing
    if (Get-Module | Where-Object {$_.Name -eq $m}) {
        LogMessage -message "MODULE $m IS ALREADY IMPORTED..."
    }
    else {

        # If module is not imported, but available on disk then import
        if (Get-Module -ListAvailable | Where-Object {$_.Name -eq $m}) {
            Import-Module $m -Verbose
        }
        else {

            # If module is not imported, not available on disk, but is in online gallery then install and import
            if (Find-Module -Name $m | Where-Object {$_.Name -eq $m}) {
                Install-Module -Name $m -Force -Verbose -Scope CurrentUser
                Import-Module $m -Verbose
            }
            else {

                # If module is not imported, not available and not in online gallery then abort
                LogMessage -message "MODULE $m NOT IMPORTED, NOT AVAILABLE AND NOT IN ONLINE GALLERY, EXITING."
                EXIT 1
            }
        }
    }
}