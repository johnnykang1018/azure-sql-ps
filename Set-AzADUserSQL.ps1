﻿Param(

    [Parameter(Mandatory=$true)]
    [string]$AzADUser,

    [Parameter(Mandatory=$true)]
    [string]$AzADAppId,

    [Parameter(Mandatory=$true)]
    [string]$SQLServerName,

    [Parameter(Mandatory=$true)]
    [string]$databaseName,

    [Parameter(Mandatory=$false)]
    [string]$scopes = "https://database.windows.net/.default"
)

Import-Module .\AzSQLCore.psm1 -Force -WarningAction SilentlyContinue

LogMessage -message "STARTING..."

Load-Module -m MSAL.PS

$token = (az account get-access-token --scope $scopes --query accessToken --output tsv)

$conn = Get-SQLConnectionString -SQLServerName $SQLServerName -databaseName $databaseName -token $token

$appServiceServicePrincipal = $AzADAppId

Create-AzADUserAsDBOwner -conn $conn -appServiceServicePrincipal $appServiceServicePrincipal -AzADUser $AzADUser

LogMessage -message "COMPLETED..."